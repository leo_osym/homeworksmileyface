package com.primeiro.homeworksmileyface

import android.content.Context
import android.graphics.*
import android.util.AttributeSet
import android.view.View

class SmileyFaceView(context: Context, attributes: AttributeSet)
    : View(context, attributes)
{
    var purple = Paint()
    var yellow = Paint()
    var blue = Paint()
    var black = Paint()
    var red = Paint()

    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)
        if(canvas == null) return


        purple.setARGB(255,255,0,255)
        yellow.color = Color.YELLOW
        blue.color = Color.BLUE
        black.color = Color.BLACK
        red.color = Color.RED

        black.typeface = Typeface.create(Typeface.MONOSPACE, Typeface.BOLD)
        black.textSize = 70f


        // face
        canvas.drawOval(RectF(100f,100f,700f,700f), yellow)
        // nose
        canvas.drawOval(RectF(380f,380f,420f,420f), black)
        // eyes
        canvas.drawOval(RectF(250f,250f,320f,320f),blue)
        canvas.drawOval(RectF(480f,250f,550f,320f),blue)
        // mouth
        canvas.drawRect(RectF(300f,500f,500f,550f),red)
        // text
        canvas.drawText("Something here",800f,900f, black)
    }
}